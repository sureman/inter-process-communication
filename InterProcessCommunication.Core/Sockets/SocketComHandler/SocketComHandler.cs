﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using InterProcessCommunication.Core.Extensions;

namespace InterProcessCommunication.Core.Sockets
{
    public class DataReceivedEventArgs<T>
    {
        public string ErrorMsg;
        public T Data;
    }

    public class BinarySocketComHandler : SocketComHandler<byte[]>
    {
        private readonly BinaryReader binaryReader;
        private readonly BinaryWriter binaryWriter;

        public int BufferSize { get; set; }

        public BinarySocketComHandler(TcpClient client, int bufferSize = 1024) : base(client)
        {
            BufferSize = bufferSize;
            binaryReader = new BinaryReader(stream);
            binaryWriter = new BinaryWriter(stream);
        }

        public override void Send(object message)
        {
            try
            {
                if (!IsConnected)
                    return;

                if (message is byte[] bytes)
                {
                    binaryWriter.Write(bytes);
                }
                else
                {
                    BinaryFormatter bf = new BinaryFormatter();
                    using (var ms = new MemoryStream())
                    {
                        bf.Serialize(ms, message);
                        binaryWriter.Write(ms.ToArray());
                    }
                }
                binaryWriter.Flush();
            }
            catch (Exception e)
            {
                Console.WriteLine($"An error occurred while sending message: {e}");
            }
        }

        protected override DataReceivedEventArgs<byte[]> CreateResponse()
        {
            var response = new DataReceivedEventArgs<byte[]>();
            try
            {
                var buffer = new byte[BufferSize];
                var length = binaryReader.Read(buffer, 0, BufferSize);
                var newBuffer = new byte[length];
                Array.Copy(buffer, newBuffer, length);
                response.Data = newBuffer;
            }
            catch (Exception e)
            {
                response.ErrorMsg = e.Message;
            }
            if (!IsConnected || response.Data.Length == 0)
                throw new ConnectionClosedException();
            return response;
        }
    }


    public class JsonSocketComHandler<T> : SocketComHandler<T>
    {
        private readonly StreamReader streamReader;
        private readonly StreamWriter streamWriter;
        public JsonSocketComHandler(TcpClient client) : base(client)
        {
            streamReader = new StreamReader(stream);
            streamWriter = new StreamWriter(stream);
        }

        public override void Send(object message)
        {
            if (IsConnected)
            {
                streamWriter.WriteLine(message.JsonSerializeToString());
                streamWriter.Flush();
            }
        }
        protected override DataReceivedEventArgs<T> CreateResponse()
        {
            var response = new DataReceivedEventArgs<T>();
            try
            {
                response.Data = streamReader.BaseStream.JsonDeserializeFromStream<T>();
            }
            catch (Exception e)
            {
                response.ErrorMsg = e.Message;
            }
            if (!IsConnected || response.Data == null) 
                throw new ConnectionClosedException();
            return response;
        }
    }

    public class ConnectionClosedException : Exception
    {

    }

    // T -> The type of the messages received

    // The purpose of this class is to listen to a stream and read any input that is sent over it. Also provides
    // method to send data. Used by the SocketClient object and is used in SocketServer to handle client connections.
    public class SocketComHandler<T> : ISocketComHandler<T>
    {
        private readonly TcpClient client;

        protected bool isHandling;
        protected readonly NetworkStream stream;
        public bool IsConnected { get => client.Connected; }

        public event EventHandler<DataReceivedEventArgs<T>>  OnDataReceived;
        public event EventHandler OnDisconnected;

        public SocketComHandler(TcpClient client)
        {
            this.client = client;
            stream = client.GetStream();
            isHandling = false;
        }

        public virtual void Send(object message)
        {
            throw new NotImplementedException();
        }

        public void Handle()
        {
            if (!isHandling && IsConnected)
            {
                isHandling = true;
                new Thread(new ThreadStart(ThreadProc)).Start();
            }
        }

        private void ThreadProc()
        {
            while (isHandling)
            {
                if (IsConnected)
                {
                    try
                    {
                        OnDataReceived?.Invoke(this, CreateResponse());
                    }
                    catch (Exception e)
                    {
                        isHandling = false;
                        if (e is ConnectionClosedException)
                            OnDisconnected?.Invoke(this, new EventArgs());
                    }
                }
                else
                    isHandling = false;
            }
        }

        protected virtual DataReceivedEventArgs<T> CreateResponse()
        {
            throw new NotImplementedException();
        }
    }
}
