﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace InterProcessCommunication.Core.Sockets.SocketComHandler
{
    public interface ISocketComHandlerFactory
    {
        ISocketComHandler<T> Create<T>(TcpClient client);
    }

    public class SocketComHandlerFactory : ISocketComHandlerFactory
    {
        public ISocketComHandler<T> Create<T>(TcpClient client)
        {
            if (typeof(T) == typeof(byte[]))
            {
                return (ISocketComHandler<T>) new BinarySocketComHandler(client);
            }
            return new JsonSocketComHandler<T>(client);
        }
    }
}
